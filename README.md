# README #

This script was created to automate some basic enumeration tasks on a windows host. If there is no python interpreter available you can precompile this script with pyinstaller to create an exe of this script. To create a standalone application you can run pyinstaller like this:
```
pyinstaller winenum.py --onefile
```

## License ##
This script is licensed under the GNU General Public License in version 3. See http://www.gnu.org/licenses/ for further details.


## Usage ##

If you have a python interpreter on the windows host, you can execute the script like this.
```
python winenum.py -a
```
If you have a precompiled version of winenum, you can run it like this:
```
winenum -a
```