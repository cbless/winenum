#!/usr/bin/env python


# Copyright (C) 2015 Christoph Bless
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Description:
# This script was created to automate some basic enumeration tasks on a windows host. If there is no python interpreter
# available you can precompile this script with pyinstaller to create an exe of this script. Run pyinstaller like
# this:
#
#     pyinstaller winenum.py --onefile
#
# Execute the script like:
#     python winenum.py -a
# or as precomiled exe
#     winenum -a
#
#
# This script is available on bitbucket.org see:
#     https://bitbucket.org/cbless/winenum

import argparse
import subprocess

version = "0.1"

# list of default commands which will always be executed
default_cmds = [
    "hostname",
    "systeminfo",
    "set",
    "arp -a",
    "ipconfig /all",
    "ipconfig /displaydns",
    "route print",
    "netstat -ano",
    "net user",
    "net user /domain",
    "net view",
    "net view /domain",
    "net accounts",
    "net accounts /domain",
    "net share",
    "net session",
    "net localgroup",
    "net localgroup Administrators",
    "net localgroup Administratoren",
    "net localgroup Administrateurs",
    "net group",
    "net group 'Domain Admins'",
    "netsh firewall show state",
    "netsh firewall show config",
    "tasklist /svc",
    "tasklist /m",
    "schtasks /query /fo LIST /v",
    "gpresult /SCOPE COMPUTER /Z",
    "gpresult /SCOPE USER /Z",
    "net start",
    "DRIVERQUERY"
]

# list of wmic commands which will be executed if the parameter -w is specified
wmic_cmds = [
    "wmic qfe get Caption,Description,HotFixID,InstalledOn",
    "wmic useraccount list",
    "wmic group list",
    "wmic service list brief",
    "wmic volume list brief",
    "wmic logicaldisk get description,filesystem,name,size",
    "wmic netlogin get name,lastlogon,badpasswordcount",
    "wmic netclient list brief",
    "wmic netuse get name,username,connectiontype,localname",
    "wmic share get name,path",
    "wmic nteventlog get path,filename,writeable",
    "wmic process list brief",
    "wmic product get name,version",
    "wmic qfe"
]

# list of password search commands which will be executed if the parameter -p is specified
password_cmds = [
    "reg query HKLM /f password /t REG_SZ /s",
    "reg query HKCU /f password /t REG_SZ /s"
]


def execute(cmd):
    """
    This function executes the given command.
    :param cmd: Command to execute
    :type cmd: string
    :return: Output of the command as string
    :rtype: string
    """
    cmd_and_params = cmd.split(" ")
    cmd_and_params.insert(0, "/c")
    cmd_and_params.insert(0, "cmd")
    process = subprocess.Popen(cmd_and_params, stdout=subprocess.PIPE)
    o,e = process.communicate()
    return o


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog="winenum", version = version,
                                     description="Winenum is a tool which is used for enumeration on a windows host.")
    parser.add_argument("-a", "--all-cmds", required=False, action='store_true',
                        help="Run all commands")
    parser.add_argument("-p", "--password-cmds", required=False, action='store_true',
                        help="Run commands to search for passwords")
    parser.add_argument("-w", "--wmic-cmds", required=False, action='store_true',
                        help="Run wmic commands")
    parser.add_argument("-f", "--file", required=False, default=None,
                        help="Filename used to store results")
    args = parser.parse_args()

    cmds = [] # The cmd list contains all the commands that will be executed
    cmds.extend(default_cmds) # add the default commands to the cmd list

    if (args.wmic_cmds is True) or (args.all_cmds is True):
        # -w or -a was given as parameter. Add the wmic commands to the cmd list
        cmds.extend(wmic_cmds)
    if (args.password_cmds is True) or (args.all_cmds is True):
        # -p or -a was given as parameter. Add the password search commands to the cmd list
        cmds.extend(password_cmds)

    filename = args.file # get the given filename
    if filename is None:
        # if no filename was specified a filename is generated. This starts with a date and time string and ends
        # with -winenum
        import time
        import datetime
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime("%Y-%m-%d-%H-%M-%S")
        filename = "{0}-winenum".format(st)

    # execute all commands in the cmd list
    for cmd in cmds:
        out = [] # this list will store the output of the command
        # add a header the each output
        out.append("#" * 79)
        out.append("# {0}".format(cmd))
        out.append("#" * 79)
        out.append("")
        try:
            # then execute the command and add the output the the output list
            o = execute(cmd)
            out.append(o)
        except Exception as e:
            out.append("")
        out_str = "\n".join(out) # generate the output string
        print "running cmd: {0}".format(cmd) # print a info message with the command name
        # write output to file
        with open(filename,"a") as f:
            f.write(out_str)
